﻿using System;

namespace ConsoleApp2
{
    public class Parameters
    {
        public void ParametersMethod(params int[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                Console.WriteLine($"Parameter {i} equals {parameters[i]}");
            }
        }
    }
}
