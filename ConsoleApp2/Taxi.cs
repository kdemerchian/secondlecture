﻿namespace ConsoleApp2
{
    public class Taxi
    {
        public bool IsInitialized;
        public string TaxiName;
        public decimal PricePerHour;

        public Taxi()
        {
            IsInitialized = true;
            TaxiName = "Uber";
            PricePerHour = 56.7m;
        }

        public Taxi(bool IsInitialized, 
            string taxiName, 
            decimal pricePerHour)
        {
            IsInitialized = this.IsInitialized;
            TaxiName = taxiName;
            PricePerHour = pricePerHour;
        }
    }
}
