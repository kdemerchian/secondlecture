﻿namespace ConsoleApp2
{
    public class Circle
    {
        private string _message;
        public int Radius { get; set; }
        public int Diameter => Radius * 2;

        public string TextMessage
        {
            get
            {
                if (!string.IsNullOrEmpty(_message))
                {
                    return _message;
                }
                if (Diameter >= 5 && Diameter <= 10)
                {
                    return "Text 1";
                }
                if (Diameter > 10  && Diameter <= 20)
                {
                    return "Text 2";
                }
                else
                {
                    return "Text3";
                }
            }
            set => _message = value;
        }
    }
}
