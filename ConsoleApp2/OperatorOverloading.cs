﻿using System;

namespace ConsoleApp2
{
    public class OperatorOverloading
    {
        public void Method() { Console.WriteLine("First Method"); }

        public void Method(int param) { Console.WriteLine("Second Method"); }

        public void Method(int param1, int param2) { Console.WriteLine("Third Method"); }

        public void Method(string stringParam) { Console.WriteLine("Fourth Method"); }
    }
}
