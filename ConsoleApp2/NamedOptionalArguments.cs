﻿using System;

namespace ConsoleApp2
{
    public class NamedOptionalArguments
    {
        public void Method1(int param1, int param2)
        {
            Console.WriteLine($"Param1 is required");
            Console.WriteLine($"Param2 is required");
        }

        public void Method2(int param1, int param2 = 10)
        {
            Console.WriteLine($"Param1 is required");
            Console.WriteLine($"Param2 is optional");
        }
    }
}
