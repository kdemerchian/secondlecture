﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            OperatorOverloading program = new OperatorOverloading();
            program.Method();
            program.Method(1);
            program.Method(1, 2);
            program.Method("test");
        }
    }
}
