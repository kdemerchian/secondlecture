﻿using System;

namespace ConsoleApp2
{
    public class Methods
    {
        public void Method1()
        {
            Console.WriteLine("We call Method1");
        }

        public void Method2(int a, int b)
        {
            int c = a + b;
            Console.WriteLine($"We call Method2. See value '{c}'");
        }

        public int Method3() //bool, string, etc
        {
            int c = 3 + 4;
            Console.WriteLine($"We call Method3. See value '{c}'");
            return c;
        }

        public int Method4(int a, int b)
        {
            int c = a + b;
            Console.WriteLine($"We call Method4. See value '{c}'");
            return c;
        }
    }
}
